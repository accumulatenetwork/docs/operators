# Configuration

This is documentation for the new configuration mechanisms released in 1.3 Baikonur.

## Migrating

TODO. TL;DR: Run `accumulate migrate`. Probably use `--pretend` to test the migration first, and then `--try` to launch with the new configuration before writing it. Pass the node path as the first argument, or with `--work-dir [path]`.

## Overriding

Configurations such as `[[configurations]] type = "core-validator"` will generate many services, but those service configurations are not written to the config file to keep it clean. If you wish to override parameters of a specific service, you can add a service configuration that matches the generated service to override specific configuration values. For example, to override the DNN's database path, add:

```toml
[[services]]
  type = "storage"
  name = "Directory"
  [services.storage]
    type = "leveldb"
    path = "dnn/data/accumulate.db"
```

Devnet configurations use subnodes. To override configuration for a devnet node, add a subnode service configuration that matches the targeted subnode. For example, to enable snapshots for the bvn1-1 node:

```toml
[[services]]
  type = "subnode"
  name = "bvn1-1"
  [[services.services]]
    type = "snapshot"
    directory = "dnn/snapshots"
    partition = "Directory"
    storage = "Directory"
```

