# Table of contents

* [Operators](README.md)
* [Configuration](configuration.md)

## Troubleshooting

* [Debug](troubleshooting/debug-tool/README.md)
  * [Database manipulation](troubleshooting/debug-tool/debug-database.md)
  * [Healing](troubleshooting/debug-tool/debug-healing.md)
  * [Network Status](troubleshooting/debug-tool/debug-net-status.md)

## AccMan

* [Update the node configuration](accman/update-the-node-configuration.md)
* [Restart the node](accman/restart-the-node.md)

## From source

* [Core node](from-source/core-node.md)

## Staking

* [Manual staking](staking/manual-staking.md)

## Testnets

* [Regenesis](testnets/regenesis.md)
