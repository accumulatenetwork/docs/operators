# Rolling back a bad block

Using the `tendermint` command from the `callum/app-version` branch of the Tendermint repository, run:

```shell
tendermint rollback --home /path/to/node
```

In a dual mode setup, the node path must point to either the `dnn` or `bvnn` folder.

This may work by itself, otherwise you may have to change the app version (TODO: document that). The command to do so is available on the aforementioned branch.