# Network status

## Check connectivity

> `debug check-node ${DOMAIN_OR_IP}`

Checks connectivity of a core validator node and lists the services it provides.

```shell
$ debug check-node apollo-mainnet.accumulate.defidevs.io
Peer ID:        12D3KooWAgrBYpWEXRViTnToNmpCoC3dvHdmR6m1FmyKjDn1NYpj
Network:        MainNet
Validator:      bc79dbf768f828beae676d6a0f76885b9371918bf08465e7a63b5ca53e27815b
Operator:       acc://defidevs.acme

✔ Can connect
✔ Can query

Has node
Has consensus:apollo
Has consensus:directory
Has network:apollo
Has network:directory
Has metrics:apollo
Has metrics:directory
Has query:apollo
Has query:directory
Has event:apollo
Has event:directory
Has submit:apollo
Has submit:directory
Has validate:apollo
Has validate:directory
Has f001:apollo
Has f001:directory
```

## Scan

> `debug network scan ${NETWORK_ENDPOINT}`

Scans the network, building a list of all nodes. Primarily used for populationg
"cached network scan" files which can be used by other commands to avoid needing
to scan the network themselves: `debug network scan kermit --json >
~/.accumulate/cache/kermit.json`.

```shell
$ debug network scan kermit
2024/04/22 13:01:11 INFO Scanning the network
2024/04/22 13:01:12 INFO Finding peers for partition=Chico
2024/04/22 13:01:12 INFO Getting identity of peer=12D3KooWDs1kumA7XK5A51Y1KqeBagoYujTvbgbfKuG7ckWAXANn
2024/04/22 13:01:12 INFO Finding peers for partition=Directory
2024/04/22 13:01:12 INFO Getting identity of peer=12D3KooWJxy1qRjPcgNjCdjkMiNembNwt3oXr4GYKgp3Bigc3Gsz
2024/04/22 13:01:12 INFO Getting identity of peer=12D3KooWDs1kumA7XK5A51Y1KqeBagoYujTvbgbfKuG7ckWAXANn
2024/04/22 13:01:12 INFO Getting identity of peer=12D3KooWErvpeKg6EjTv8rkndzaW26JRPRYsw3R6N6ABE1jBcnNL
2024/04/22 13:01:12 INFO Finding peers for partition=Groucho
2024/04/22 13:01:13 INFO Getting identity of peer=12D3KooWJxy1qRjPcgNjCdjkMiNembNwt3oXr4GYKgp3Bigc3Gsz
2024/04/22 13:01:13 INFO Finding peers for partition=Harpo
2024/04/22 13:01:13 INFO Getting identity of peer=12D3KooWErvpeKg6EjTv8rkndzaW26JRPRYsw3R6N6ABE1jBcnNL
Chico
Directory
Groucho
Harpo
```

## Check network status

> `debug network status ${NETWORK_ENDPOINT}`

Scans the network (unless --cached-scan is provided) and reports the status of
all nodes.

```shell
$ debug network status kermit
Identifying validators...
Checking peers...
Checking versions...
Checking API v3...
Checking consensus...
+--------------+----------+------------------------------------+-----------------------+--------+--------------+--------------+--------------+--------------+
| VALIDATOR ID | OPERATOR |                HOST                |        VERSION        | API V3 |  DIRECTORY   |    CHICO     |   GROUCHO    |    HARPO     |
+--------------+----------+------------------------------------+-----------------------+--------+--------------+--------------+--------------+--------------+
| 077ebe0b     |          | kermit-bvn1.accumulate.defidevs.io | v1.3.0-191-g4f721ab8f | ✔ ✔    | 🔑 2460702   |              |              | 🔑 3145472   |
| 892d87b9     |          | kermit-bvn0.accumulate.defidevs.io | v1.3.0-191-g4f721ab8f | ✔ ✔    | 🔑 2460702   | 🔑 3100143   |              |              |
| 950e8118     |          | kermit-bvn2.accumulate.defidevs.io | v1.3.0-191-g4f721ab8f | ✔ ✔    | 🔑 2460702   |              | 🔑 2917177   |              |
+--------------+----------+------------------------------------+-----------------------+--------+--------------+--------------+--------------+--------------+
```

## Scan a node

> `debug network scan-node ${NODE_ADDRESSS}`

Similar to [Scan](#scan) but scans a single node instead of the entire network.
