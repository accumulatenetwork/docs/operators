# Debug tool

The [core repository][core] includes a program, `debug`, which includes many
tools for maintaining and debugging the network.

[core]: https://gitlab.com/accumulatenetwork/accumulate

## Build

1. Checkout [accumulate][core]
2. `cd accumulate`
3. `go build ./tools/cmd/debug`

## Healing

Tools for diagnosing and healing sequencing issues (anchors and synthetic
transactions). See [Healing](./debug-healing.md).

## Database

Database and snapshot maniuplation tools. See [Database](./debug-database.md).

## Network status

Tools for scanning the network and checking it's health. See [Network
status](./debug-net-status.md).

## Routing

> `debug accumulate route ${NETWORK_ENDPOINT} ${ACCOUNT_URL}`

Displays information about how an account url is routed to a BVN of the given
network. It is not necessary for the URL to refer to an existing account.

```
$ debug account route mainnet foo.bar
Account:        acc://foo.bar
Account ID:     2595D08AD22C733F7A1CE713E767563E13A8DFA35BAA74919C28E0F586CB424B
Identity ID:    2595D08AD22C733F7A1CE713E767563E13A8DFA35BAA74919C28E0F586CB424B
Routing number: 2595D08AD22C733F
Method:         prefix routing table
Routes to:      Apollo
```

The last line, "Routes to: Apollo", specifies which BVN the account routes to.
The previous lines are implementation details.

## Encoding, decoding, and hashing

> `debug encode [transaction|signature|envelope] "${JSON}"`
> `debug encode --hash [transaction|signature] "${JSON}"`
> `debug decode [transaction|signature|envelope] "${BIN}"`

Encodes a transaction, signature, or envelope expressed as JSON into binary
(encode) or vice versa (decode). Calculates the hash of a transaction or
signature (encode --hash). The payload can be passed as the first (and only)
argument, or via stdin.

## Signing

> `debug sign "${JSON_SIGNATURE}" ${PRIVATE_KEY}`

Completes the signing process (TODO: details of signing).

> `debug verify "${JSON_ENVELOPE}"`

Verifies/validates signatures within an envelope.
