# Healing

The mechanisms for anchoring partitions and for sending synthetic transactions
may occasionally need help. In general these mechanisms should self-heal, but
they may occasionally require manual intervention. If anchoring between
partitions, synthetic transactions, or signature forwarding appear to be stuck,
the following tools may resolve the issue.

## Scan the network

Healing tools work faster and more reliably when a cached network scan is
available. Scan the network and save the result:

> `debug network scan ${NETWORK_ENDPOINT} --json > ~/.accumulate/cache/${NETWORK_NAME}.json`

By convention, the network scan should be written to a JSON file within
`~/.accumulate/cache`, where the filename is the network name, in lower case
(plus `.json`). Besides being a useful convention, some tools will automatically
use the cached scan if an appropriately named file exists.

## Check sequencing

Verify that there is a sequencing issue:

> `debug sequence ${NETWORK_ENDPOINT}`

If everything is good, you should see lots of check marks:

```shell
$ debug sequence mainnet
We are 12D3KooWEonEyJn5qepvMRPZRvbEdn2Pyftwe6fHAeXbTePPouSM
Network status
Query Apollo
2024/04/22 13:33:58 INFO Got account url=acc://bvn-Apollo.acme/anchors lastBlockAge=5s
2024/04/22 13:33:58 INFO Got account url=acc://bvn-Apollo.acme/synthetic lastBlockAge=5s
2024/04/22 13:33:58 INFO Got account url=acc://bvn-Apollo.acme/anchors lastBlockAge=5s
Query Chandrayaan
2024/04/22 13:35:23 INFO Got account url=acc://bvn-Chandrayaan.acme/anchors lastBlockAge=4s
2024/04/22 13:35:23 INFO Got account url=acc://bvn-Chandrayaan.acme/synthetic lastBlockAge=4s
2024/04/22 13:35:23 INFO Got account url=acc://bvn-Chandrayaan.acme/anchors lastBlockAge=4s
Query Directory
2024/04/22 13:35:24 INFO Got account url=acc://dn.acme/anchors lastBlockAge=3s
2024/04/22 13:35:24 INFO Got account url=acc://dn.acme/synthetic lastBlockAge=3s
2024/04/22 13:35:24 INFO Got account url=acc://dn.acme/anchors lastBlockAge=4s
2024/04/22 13:35:24 INFO Got account url=acc://dn.acme/anchors lastBlockAge=4s
2024/04/22 13:35:24 INFO Got account url=acc://dn.acme/anchors lastBlockAge=4s
2024/04/22 13:35:25 INFO Got account url=acc://dn.acme/anchors lastBlockAge=2s
Query Yutu
2024/04/22 13:35:25 INFO Got account url=acc://bvn-Yutu.acme/anchors lastBlockAge=9s
2024/04/22 13:35:25 INFO Got account url=acc://bvn-Yutu.acme/synthetic lastBlockAge=3s
2024/04/22 13:35:26 INFO Got account url=acc://bvn-Yutu.acme/anchors lastBlockAge=3s
✔ Apollo → Apollo
✔ Apollo → Chandrayaan
✔ Apollo → Directory
✔ Apollo → Yutu
✔ Chandrayaan → Apollo
✔ Chandrayaan → Chandrayaan
✔ Chandrayaan → Directory
✔ Chandrayaan → Yutu
✔ Directory → Apollo
✔ Directory → Chandrayaan
✔ Directory → Directory
✔ Directory → Yutu
✔ Yutu → Apollo
✔ Yutu → Chandrayaan
✔ Yutu → Directory
✔ Yutu → Yutu
```