# Database and snapshot tools

These tools use a custom URL format to indicate the type and location of the
database(s) being manipulated, and to allow remote database connections.

- Local - `{type}://{path}`
  - The type and location of a local database, e.g. `badger:///path/to/badger.db`.
  - Supported types: badger, leveldb, bolt.
- Snapshot - `snapshot://{path}` - read-only
  - Treats a snapshot as a database.
- Remote - `{proto}://{address}:{port}`
  - Connects to a remote database or serves a database for remote clients
    (depending on the command and usage).

> `debug database clone ${SRC} ${DST}`

Clones a database. The destination must be empty or non-existent.

> `debug database serve ${LOCAL} ${REMOTE}`

Serves a database **with no authentication**, allowing remote clients to read
and write records. Example: `debug db serve badger:///path/to/source.db
unix:///path/to/serve.sock`.

> `debug database sync ${SRC} ${DST}`

Synchronizes the destination with the source, inserting, deleting, and
overwriting records as necessary.

> `debug snapshot collect ${DB} ${SNAPSHOT}`

Collects a snapshot of a database.

> `debug snapshot dump ${SNAPSHOT}`

Dumps the contents of a snapshot or genesis file, for debugging.

> `debug snapshot index ${SNAPSHOT}`

Indexes the records of a snapshot so it can be used as a (read-only) database.

> `debug snapshot restore ${SNAPSHOT} ${DB}`

Restores a snapshot to a database. The destination must be empty or
non-existent.