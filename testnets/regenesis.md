# Regenesis

'Regenesis' refers to reconstructing a testnet with a new genesis document that preserves account data.

### Procedure

1. Capture a snapshot from each BVN:
   * `debug snapshot collect type-of-db:///path/to/db.db /path/to/snapshot.snap --skip-bpt --skip-system`
2. Optionally, capture a snapshot from the DN (same command as above, with different arguments).
3. Copy the snapshots to your PC.
4. Consolidate the snapshots and strip unused records:
   * `accumulated init prepare-genesis output.snap input1.snap input2.snap ...`
5. Construct or retrieve a network description file.
6. Construct new genesis documents:
   * `accumulated init genesis network.json -w <output-dir> --snapshot output.snap`
7. Copy the new genesis files to the servers.
8. Execute the `NUKE-STATE.sh` maintenance script.
9. Restart the nodes.

### Kermit

Kermit's current network definition is:

```json
{
    "id": "Kermit",
    "globals": {
        "executorVersion": "v2-baikonur",
        "oracle": {
            "price": 500000
        },
        "globals": {
            "feeSchedule": {
                "createIdentitySliding": [
                    400000
                ]
            },
            "limits": {
                "identityAccounts": 1000
            }
        },
        "routing": {
            "routes": [
                {
                    "length": 2,
                    "partition": "Chico"
                },
                {
                    "length": 2,
                    "partition": "Harpo",
                    "value": 1
                },
                {
                    "length": 2,
                    "partition": "Groucho",
                    "value": 2
                },
                {
                    "length": 3,
                    "partition": "Chico",
                    "value": 6
                },
                {
                    "length": 4,
                    "partition": "Harpo",
                    "value": 14
                },
                {
                    "length": 4,
                    "partition": "Groucho",
                    "value": 15
                }
            ]
        }
    },
    "bvns": [
        {
            "id": "Chico",
            "nodes": [
                {
                    "dnnType": "validator",
                    "bvnnType": "validator",
                    "basePort": 16591,
                    "advertizeAddress": "bvn0.kermit.accumulate.defidevs.io",
                    "hostName": "bvn0.kermit.accumulate.defidevs.io",
                    "privValKey": "...",
                    "dnNodeKey": "...",
                    "bvnNodeKey": "..."
                }
            ]
        },
        {
            "id": "Harpo",
            "nodes": [
                {
                    "dnnType": "validator",
                    "bvnnType": "validator",
                    "basePort": 16591,
                    "advertizeAddress": "bvn1.kermit.accumulate.defidevs.io",
                    "hostName": "bvn1.kermit.accumulate.defidevs.io",
                    "privValKey": "...",
                    "dnNodeKey": "...",
                    "bvnNodeKey": "..."
                }
            ]
        },
        {
            "id": "Groucho",
            "nodes": [
                {
                    "dnnType": "validator",
                    "bvnnType": "validator",
                    "basePort": 16591,
                    "advertizeAddress": "bvn2.kermit.accumulate.defidevs.io",
                    "hostName": "bvn2.kermit.accumulate.defidevs.io",
                    "privValKey": "...",
                    "dnNodeKey": "...",
                    "bvnNodeKey": "..."
                }
            ]
        }
    ]
}
```
