# Core node

## Dual mode

```bash
# Clone the repository
git clone https://gitlab.com/accumulatenetwork/accumulate.git
cd accumulate
git checkout v1.2.9

# Build the binary
go build ./cmd/accumulated
sudo cp accumulated /usr/local/bin/

# Initialize the node
DIR=/node
PARTITION=apollo # Options: apollo, yutu, chandrayaan
PUBLIC=my-node.accumulate.example.com
accumulated init dual tcp://${PARTITION}-mainnet.accumulate.defidevs.io:16691 -w $DIR --public $PUBLIC

# Fix port numbers (v1.2.x only)
cd /path/to/repo/
git checkout main
./scripts/ops/fix/fix-bvn-ports.sh

# Launch the node
accumulated run-dual $DIR/dnn $DIR/bvnn
```

## Directory only

Follow the same instructions as above with the following changes:

```sh
# Initialize the node
#   Run 'init node' instead of 'init dual' and use port 16591 instead of 16691
#   The partition is irrelevant in this mode
accumulated init node tcp://apollo-mainnet.accumulate.defidevs.io:16591 -w $DIR --public $PUBLIC

# Skip "Fix port numbers"

# Launch the node
accumulated run -w $DIR/dnn
```
