# Update the node configuration

1. Enter the **Manage node** menu\
   ![](<../.gitbook/assets/image (10).png>)
2. Attach to the BASH console\
   ![](<../.gitbook/assets/image (11).png>)
3. Execute `nano /node/path/to/file.toml`\
   ![](<../.gitbook/assets/image (12).png>)
   * `/node/dnn/accumulate.toml` is the DNN's Accumulate configuration
   * `/node/bvnn/accumulate.toml` is the BVNN's Accumulate configuration
4. Edit the file
5. Type `[Ctrl+X]` to exit nano\
   ![](<../.gitbook/assets/image (6).png>)
6. Type `y` to save changes\
   ![](<../.gitbook/assets/image (4).png>)
7. Type `[Enter]` to confirm\
   ![](<../.gitbook/assets/image (3).png>)
8. Exit BASH
9. [restart-the-node.md](restart-the-node.md "mention")
