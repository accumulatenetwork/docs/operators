# Update the node

1. Enter the **Manage node** menu\
   ![](<../.gitbook/assets/image (2).png>)
2. Stop the node\
   ![](../.gitbook/assets/image.png)
3. Confirm\
   ![](<../.gitbook/assets/image (5).png>)
4. Wait for the node to stop
5. Start the node\
   ![](<../.gitbook/assets/image (7).png>)
6. Select the appropriate version\
   ![](<../.gitbook/assets/image (1).png>)
7. Wait for the node to start
8. Exit AccMan
