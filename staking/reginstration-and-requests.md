# Registration and requests

Registering a staking account and requests to update that registration are
handled via `staking.acme/requests` and `staking.acme/registration`:

1. The user submits a [request](#requests) to `staking.acme/requests`.
2. Staking fetches the registration list from `staking.acme/registration`.
3. If the request is valid, staking submits a new or updated registration entry
   to `staking.acme/registration`.
4. If a new staking account is being registered, staking initiates an authority
   transfer transaction which must be signed by the user before staking rewards
   will be issued.

## Requests

A request is a WriteData transaction submitted to `staking.acme/requests`
containing a request and signed by the key books that own the account in
question. **The transaction must request a signature from all owner key books**
(via `authorities` in the header). If a request has not been completed, it may
be cancelled by submitting a cancel request. [Here][action-types] is the list of
recognized requests. Requests are encoded as the request type followed by its
fields as `key=value` pairs; see [here][action-format] for details.

[action-types]: https://gitlab.com/accumulatenetwork/core/staking/-/blob/main/pkg/requests/types.yml
[action-format]: https://gitlab.com/accumulatenetwork/core/staking/-/blob/00d08276c192d7f05c29c9820432c2bd81937d86/pkg/requests/action.go#L103

For example, if `foo.acme/stake` is owned by `foo.acme/book` and the owner
wishes to register it as a staking account delegated to `bar.acme` with
compounding rewards:

```json
{
    "header": {
        "principal": "acc://staking.acme/requests",
        "authorities": ["acc://foo.acme/book"]
    },
    "body": {
        "type": "writeData",
        "entry": {
            "type": "doubleHash",
            "data": [
                "addAccount",
                "type=delegated",
                "account=acc://foo.acme/stake",
                "payout=acc://foo.acme/stake",
                "delegate=acc://bar.acme",
            ]
        }
    }
}
```

Later, if the owner wishes to change the payout address to `foo.acme/rewards`:

```json
{
    "header": {
        "principal": "acc://staking.acme/requests",
        "authorities": ["acc://foo.acme/book"]
    },
    "body": {
        "type": "writeData",
        "entry": {
            "type": "doubleHash",
            "data": [
                "changePayout",
                "account=acc://foo.acme/stake",
                "destination=acc://foo.acme/rewards",
            ]
        }
    }
}
```

## Registration

The registration list (`staking.acme/registration`) is a list of JSON-encoded
data entries, one per *identity*. The only most recent entry for a given
identity takes effect. [Here][reg-types] is the type definition for registration
entries.

For example, after the two example requests above, the latest registration entry
for `foo.acme` would be:

```json
{
    "identity": "acc://foo.acme",
    "accounts": [{
        "type": "delegated",
        "url": "acc://foo.acme/stake",
        "payout": "acc://foo.acme/rewards",
        "delegate": "acc://bar.acme"
    }]
}
```

[reg-types]: https://gitlab.com/accumulatenetwork/core/staking/-/blob/main/pkg/types/types.yml